package net.sf.owt.gcollector;

import java.util.List;

import org.opencms.file.CmsDataAccessException;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.main.CmsException;

public interface I_CmsGroovyCollector {

    public String getCreateLink(CmsObject cms, String param) throws CmsException, CmsDataAccessException;
    
    public String getCreateParam(CmsObject cms, String param) throws CmsDataAccessException;
    
    public List<CmsResource> getResults(CmsObject cms, String param) throws CmsDataAccessException, CmsException;
    
}
