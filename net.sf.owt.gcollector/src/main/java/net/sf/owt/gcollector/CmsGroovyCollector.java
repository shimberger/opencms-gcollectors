package net.sf.owt.gcollector;

import groovy.lang.GroovyClassLoader;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencms.file.CmsDataAccessException;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.file.collectors.A_CmsResourceCollector;
import org.opencms.main.CmsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CmsGroovyCollector extends A_CmsResourceCollector {

    private static final String GROOVY_SUFFIX = ".groovy";

    private static final String SYSTEM_MODULES = "/system/modules";

    private static final String COLLECTORS_SUBFOLDER = "collectors";

    private static Logger LOG = LoggerFactory.getLogger(CmsGroovyCollector.class);

    private static final String COLLECTOR_NAME = "groovy";

    private static interface GroovyHandler<T> {

        public T exec(I_CmsGroovyCollector collector) throws CmsException;
        
    }
    
    public List<String> getCollectorNames() {
        return Arrays.asList(new String[] { COLLECTOR_NAME });
    }

    public String getCreateLink(final CmsObject cms, final String collectorName, final String originalParam) throws CmsException, CmsDataAccessException {
        return withGroovyCollector(cms,getGroovyCollectorName(originalParam),new GroovyHandler<String>() {
            public String exec(I_CmsGroovyCollector collector) throws CmsException {
                final String groovyParam = getGroovyCollectorParam(originalParam);
                return collector.getCreateLink(cms, groovyParam);
            }
        });
    }

    public String getCreateParam(final CmsObject cms, final String collectorName, final String originalParam) throws CmsDataAccessException {
        try {
            String groovyCollectorName = getGroovyCollectorName(originalParam);
            return groovyCollectorName + "|" + withGroovyCollector(cms,groovyCollectorName,new GroovyHandler<String>() {
                public String exec(I_CmsGroovyCollector collector) throws CmsException {
                    final String groovyParam = getGroovyCollectorParam(originalParam);
                    return collector.getCreateParam(cms, groovyParam);
                }
            });
        } catch (CmsException e) {
            throw new RuntimeException(e);
        }
    }

    
    public List<CmsResource> getResults(final CmsObject cms, final String collectorName, final String originalParam) throws CmsDataAccessException, CmsException {
        return withGroovyCollector(cms,getGroovyCollectorName(originalParam),new GroovyHandler<List<CmsResource>>() {
            public List<CmsResource> exec(I_CmsGroovyCollector collector) throws CmsException {
                final String groovyParam = getGroovyCollectorParam(originalParam);
                return collector.getResults(cms, groovyParam);
            }
        });
    }

    private String getGroovyCollectorParam(String param) {
        if (param.indexOf('|') <= 0) {
            return "";
        }
        return param.substring(param.indexOf('|') + 1);
    }

    private String getGroovyCollectorName(String param) {
        if (param.indexOf('|') < 0) {
            return param;
        }
        return param.substring(0, param.indexOf('|'));
    }

    private <T> T withGroovyCollector(CmsObject cmso, String collectorName, GroovyHandler<T> h) throws CmsException {
        Map<String, CmsResource> collectors = getCollectors(cmso);
        if (collectors.containsKey(collectorName)) {
            CmsFile groovyFile = cmso.readFile(collectors.get(collectorName));
            GroovyClassLoader gcl = new GroovyClassLoader();
            try {
                Class clazz = gcl.parseClass(new ByteArrayInputStream(groovyFile.getContents()), groovyFile.getRootPath());
                I_CmsGroovyCollector collector = (I_CmsGroovyCollector) clazz.newInstance();
                return h.exec(collector);
            } catch (InstantiationException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Error instantiating groovy collector from " + groovyFile.getRootPath(), e);
                }
            } catch (IllegalAccessException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Error instantiating groovy collector from " + groovyFile.getRootPath(), e);
                }
            } finally {
                try {
                    if (gcl instanceof Closeable) {
                        gcl.close();
                    }
                } catch (IOException e) { }
            }
        }
        throw new RuntimeException("Could not find collector with name " + collectorName + " in " + collectors.keySet());
    }
    
    private Map<String, CmsResource> getCollectors(CmsObject cmso) throws CmsException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Looking for groovy collectors");
        }
        Map<String, CmsResource> collectors = new HashMap<String, CmsResource>();
        List<CmsResource> moduleFolders = cmso.readResources(SYSTEM_MODULES, CmsResourceFilter.DEFAULT.addRequireFolder(), false);
        for (CmsResource moduleFolder : moduleFolders) {
            String collectorFolderPath = moduleFolder.getRootPath() + COLLECTORS_SUBFOLDER;
            if (LOG.isDebugEnabled()) {
                LOG.debug("Checking " + collectorFolderPath + " for groovy collectors");
            }
            if (!cmso.existsResource(collectorFolderPath)) {
                continue;
            }
            List<CmsResource> collectorResources = cmso.readResources(collectorFolderPath, CmsResourceFilter.DEFAULT.addRequireFile(), true);
            for (CmsResource collectorResource : collectorResources) {
                if (collectorResource.getName().endsWith(GROOVY_SUFFIX)) {
                    String filename = collectorResource.getName();
                    String basename = filename.substring(0, filename.lastIndexOf('.'));
                    collectors.put(basename, collectorResource);
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Found groovy collector in " + collectorFolderPath + ". Name is " + basename + " and path is "
                                + collectorResource.getRootPath() + "");
                    }
                }
            }
        }
        return collectors;
    }

}
