package net.sf.owt.gcollector;

import java.util.List;

import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.collectors.CmsCollectorData;
import org.opencms.main.CmsException;
import org.opencms.main.OpenCms;

public abstract class A_CmsGroovyCollector implements I_CmsGroovyCollector {

    /**
     * Shrinks a List to fit a maximum size.<p>
     * 
     * @param result a List
     * @param maxSize the maximum size of the List
     * 
     * @return the reduced list
     */
    protected List<CmsResource> shrinkToFit(List<CmsResource> result, int maxSize) {

        if ((maxSize > 0) && (result.size() > maxSize)) {
            // cut off all items > count
            result = result.subList(0, maxSize);
        }
        return result;
    }  
    
    /**
     * Returns the link to create a new XML content item in the folder pointed to by the parameter.<p>
     * 
     * @param cms the current CmsObject
     * @param data the collector data to use
     * 
     * @return the link to create a new XML content item in the folder
     * 
     * @throws CmsException if something goes wrong
     * 
     * @since 7.0.2
     */
    protected String getCreateInFolder(CmsObject cms, CmsCollectorData data) throws CmsException {

        return OpenCms.getResourceManager().getNameGenerator().getNewFileName(cms, data.getFileName(), 4);
    }

    /**
     * Returns the link to create a new XML content item in the folder pointed to by the parameter.<p>
     * 
     * @param cms the current CmsObject
     * @param param the folder name to use
     * 
     * @return the link to create a new XML content item in the folder
     * 
     * @throws CmsException if something goes wrong
     */
    protected String getCreateInFolder(CmsObject cms, String param) throws CmsException {

        return getCreateInFolder(cms, new CmsCollectorData(param));
    }    
    
}
