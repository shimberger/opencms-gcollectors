OpenCms Groovy Collectors
=========================

Allows modules to provide collectors as Groovy scripts.

Installation
------------

Install the module using the OpenCms Administration.

Add the following collector to your *WEB-INF/config/opencms-vfs.xml*

	<collector class="net.sf.owt.gcollector.CmsGroovyCollector" order="180" />	

Restart the OpenCms web application.

Usage
-----

Inside your module create a directory named *collectors* and create a plain text file
named *<collectorName>.groovy*.

In this file write a class following this scheme:

	import net.sf.owt.gcollector.*;
	import org.opencms.file.*;
	import org.opencms.main.*;

	public class CollectorNameCollector extends A_CmsGroovyCollector {

		public String getCreateLink(CmsObject cms, String param) throws CmsException, CmsDataAccessException {
			return null;
		}
			
		public String getCreateParam(CmsObject cms, String param) throws CmsDataAccessException {
			return null;
		}
			
		public List<CmsResource> getResults(CmsObject cms, String param) 
		throws CmsDataAccessException, 	CmsException {
			// This collector will just return all the OpenCms v8 events in the default site
			return cms.readResources("/sites/default/",CmsResourceFilter.DEFAULT.addRequireType(149),true);
		}

	}

You can then use this collector in a JSP like the following:

	<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
	<cms:contentload collector="groovy" param="<collectorName>" editable="true" >
		...
	</cms:contentload>

All the parameters after the name will be passed on. So if you do:

	<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
	<cms:contentload collector="groovy" param="<collectorName>|param1|param2" editable="true" >
		...
	</cms:contentload>

Your Groovy class will see the parameter *param1|param2*.

Disclaimer
----------

Right now the performance is not great since the collectors get looked up and parsed for every method call.
Just use the Flex Cache ;).

Building
--------

This module uses my Gradle plugin. Just cd into the root and type:

	./gradlew ocmsModule

Add a .bat if you're on Windows.


Contribution
------------

Just fork and pull request.